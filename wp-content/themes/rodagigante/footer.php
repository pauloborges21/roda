<div>
    <div class="container container-footer w-container">
        <img src="<?php bloginfo('template_directory'); ?>/images/logo-frase.png" alt="" class="logo-footer">
        <div class="box-social">
            <div class="row-social w-row">
                <div class="w-col w-col-6">
                    <a href="https://www.facebook.com/rodagigantebrinquedos" target="_blank" class="a-social w-inline-block">
                        <img src="<?php bloginfo('template_directory'); ?>/images/ico-face2.png" alt="">
                    </a>
                </div>
                <div class="w-col w-col-6">
                    <a href="https://www.instagram.com/rodagigantebrinquedos/" target="_blank" class="a-social w-inline-block">
                        <img src="<?php bloginfo('template_directory'); ?>/images/ico-insta.png" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div>Roda gigante brinquedos • Copyright © Todos os direitos reservados.</div>
    </div>
</div>
<?php wp_footer();?>
<script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/webflow.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/webflow-2.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>