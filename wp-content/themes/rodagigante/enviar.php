<?php
//Template Name: Enviar
/**
 * User: thiago.lemos
 * Date: 21/09/2018
 * Time: 16:04
 */

require_once("phpmailer/class.phpmailer.php");
require_once("phpmailer/class.smtp.php");


?>
<?php

if(($_POST['nome-contato-footer'] != '' || $_POST['nome-contato-footer'] == null) && ($_POST['email-contato-footer'] != '' || $_POST['email-contato-footer'] == null) && ($_POST['assunto-contato-footer'] != '' || $_POST['assunto-contato-footer'] == null) &&  ($_POST['mensagem-contato-footer'] != '' || $_POST['mensagem-contato-footer'] == null))

$nome       = $_POST['nome-contato-footer'];
$email      = $_POST['email-contato-footer'];
$assunto    = $_POST['assunto-contato-footer'];
$mensagem   = $_POST['mensagem-contato-footer'];

$mail = new PHPMailer();

// Define os dados do servidor e tipo de conexão
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsSMTP();
$mail->SMTPSecure = 'ssl';
$mail->Host = "smtp.gmail.com"; // Endereço do servidor SMTP
$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional) - bom ter pra não ir pra spam
$mail->Port = 465;
$mail->Username = 'fale.conosco.rai@gmail.com'; // Usuário do servidor SMTP
$mail->Password = 'R00ftop@r4i'; // Senha do servidor SMTP

// Define o remetente
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->From = $email; // Seu e-mail
$mail->FromName = $nome; // Seu nome

// Define os destinatário(s)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAddress('thiagonegro@gmail.com', 'Thiago');
$mail->AddAddress('paulosbzl@gmail.com');
//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

// Define os dados técnicos da Mensagem
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
$mail->CharSet = 'utf-8'; // Charset da mensagem

// Define a mensagem (Texto e Assunto)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
$mail->Subject = $assunto; // Assunto da mensagem
$mail->Body = 'Assunto: ' . $assunto;
$mail->Body .= '<br><br>Nome: ' . $nome;
$mail->Body .= '<br><br>E-mail: ' . $email;
$mail->Body .= '<br><br>Mensagem: ' . $mensagem;
//$mail->AltBody = "Este é o corpo da mensagem de teste, em Texto Plano! \r\n :)";

// Define os anexos (opcional)
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//$mail->AddAttachment("c:/temp/documento.pdf", "novo_nome.pdf");  // Insere um anexo

// Envia o e-mail
$enviado = $mail->Send();

// Limpa os destinatários e os anexos
$mail->ClearAllRecipients();
$mail->ClearAttachments();

// Exibe uma mensagem de resultado
if ($enviado) {
    echo '<div style="background-color: #d4edda; color: #155724; padding: .75rem 1.25rem;">Obrigado! Sua mensagem foi enviada!</div>';
} else { //echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
    echo '<div style="background-color: #f8d7da; color: #721c24; padding: .75rem 1.25rem;">Oops..tivemos um problema e a mensagem não foi enviada. Tente novamente, ou envie um e-mail para contato@rodagigantebrinquedos.com.br</div>';
}