<?php
//Template Name: Oficinas

?>
<?php get_header();?>
  <div class="section section-breadcrumb">
    <div class="container container-bread w-container">
      <ul class="ul-bread w-clearfix w-list-unstyled">
        <li class="li-bread"><?php woocommerce_breadcrumb(['delimiter' => ' > ']); ?></li>

        </ul>
    </div>
  </div>
  <div class="section">
    <div class="container container-interna w-container">
      <div class="box-banner-oficina">
        <div class="info-banner-of">
          <h1 class="h1-of">criança<br>vem<br>de<br>&quot;criaÇão&quot;</h1>
          <div>Um lençol<br>no pescoço<br>e viram heróis,<br>uma enorme<br>caixa de papelão<br>e inauguram<br>um castelo.</div>
        </div>
      </div>
      <h2 class="h2-of">Tudo é criatividade, transformação<br>e, principalmente, diversão.</h2><img src="images/banner-oficina2.jpg" srcset="images/banner-oficina2-p-500.jpeg 500w, images/banner-oficina2-p-800.jpeg 800w, images/banner-oficina2.jpg 1289w" sizes="(max-width: 767px) 100vw, (max-width: 991px) 728px, 940px" alt="" class="mid-banner-of">
      <p class="info-of">Botar a mão na massa, brincar e inventar, é o que torna mágico o universo das crianças.<br>Que tal trocar o touch screen pelo toque do coração? Na roda gigante, não damos apenas asas à imaginação:<br>nós construímos as asas e alçamos voo a territórios, universos e galáxias que só a mente de uma criança<br>pode alcançar. Oficinas de criação de brinquedos, jogos de família, música, culinária.<br>Tudo para desenvolver e alegrar a mente inquieta dos pequenos! Também temos oficinas gratuitas!</p>
      <div class="cta-of">
        <div class="text-block">Traga<br>sua criança<br>para fazer<br>parte desse<br>mundo<br>mágico.</div><a href="https://api.whatsapp.com/send?phone=5511976943311" target="_blank" class="btn-cta-of w-button">Fale com a gente pelo WhatsApp!</a></div>
    </div>
  </div>
  <div>
    <?php get_footer();?>