<!DOCTYPE html>

<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Wed Sep 18 2019 18:09:40 GMT+0000 (UTC)  -->
<html data-wf-page="5b9bfa63efe50a09690f69b9" data-wf-site="5b9a7a40c03054f6cc545866">
<head>
    <meta charset="utf-8">
    <title><?php bloginfo('name'); ?> | <?php the_title(); ?></title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="<?php bloginfo('template_directory'); ?>/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/css/webflow.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/css/roda-gigante.webflow.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/css/adicional.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/css/roda-gigante.webflow-2.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="<?php bloginfo('template_directory'); ?>/icons/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="<?php bloginfo('template_directory'); ?>/icons/webclip.png" rel="apple-touch-icon">
    <style type="text/css">
        @media screen and (min-width: 1280px) {
            .w-container {
                max-width: 1640px;
            }
            .nav-menu {
                font-size: 18px;
            }
            .nav-link {
                margin-right: 30px;
                margin-left: 30px;
            }
            .li-icos-nav {
                margin-right: 20px;
                margin-left: 20px;
                font-size: 13px;
                font-weight: 600;
            }
            .container.container-main-banner {
                background-position: 50% 65%;
            }
            .logo-main-banner {
                width: 450px;
            }
            .box-institu {
                height: 450px;
            }
            .content-institu.fixo {
                font-size: 20px;
                line-height: 30px;
                letter-spacing: 15px;
            }
            h2 {
                font-size: 22px;
                line-height: 40px;
                font-weight: 500;
                color: #707070;
                letter-spacing: 5px;
            }
            .content-institu {
                padding-top: 110px;
                font-size: 15px;
                line-height: 20px;
            }
            .h2-sustentabilidade {
                letter-spacing: 10px;
                font-size: 20px;
            }
            .txt-slide {
                font-size: 15px;
                line-height: 17px;
            }
            .box-dstq-left, .box-dstq-right {
                height: 730px;
            }
            .dstq-g {
                height: 450px;
                background-position: 50% 50%;
                background-size: auto 350px;
            }
            .img-dstq {
                max-width: 100%;
                width: 70%;
            }
            .hover-nome-prod {
                font-size: 20px;
                line-height: 40px;
            }
            .dstq-p {
                height: 280px;
            }
            .dstq-g2 {
                height: 380px;
            }
            .dstq-p2 {
                height: 350px;
            }
            .content-prod-det {
                width: 55%;
            }
            .box-dif {
                line-height: 50px;
                font-weight: 500;
                font-size: 20px;
                letter-spacing: 3px;
                height: 325px;
            }
            .box-banner-oficina {
                background-size: 60%;
            }
            .info-of {
                font-size: 22px;
                line-height: 42px;
            }
            .cta-of {
                background-position: 100% 0%;
                background-size: 60%;
            }
        }
        select {
            -webkit-appearance: none !important; /*Removes default chrome and safari style*/
            -moz-appearance: none !important; /*Removes default style Firefox*/
            -ms-appearance: none;
            -o-appearance:none !important;
            appearance:none !important;
            overflow:hidden;
            width: 120%;
        }
        select::-ms-expand {
            display: none !important;
        }
        .select-cadastro::-ms-expand {
            display: none !important;
        }
        .slide-nav-prods .w-slider-dot {
            background-color: #e9e9e9;
        }
        .slide-nav-prods .w-slider-dot.w-active {
            background-color: #00d1e8;
        }
    </style>
    <?php wp_head(  );
    $cart_count = WC()->cart->get_cart_contents_count(); // estamos chamando o objeto WC e função de contagem de itens

    /*
    Puxa os menus pelo WP
    wp_nav_menu([
        menu =>'categoria',
        menu =>'contato',
        'container' => 'nav',
        'container' => 'nav-menu w-nav-menu',



       ]);

        */
 
    
    ?>
</head>
<body>

<div class="section section-header-interna">
    <div class="container w-container">
        <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
            <div class="container-nav container-nav-interna w-container">
                <a href="<?php echo site_url(); ?>/" class="logo-nav-interna w-inline-block">
                    <img src="<?php bloginfo('template_directory'); ?>/images/logo-footer.png" alt="">
                </a>
                <nav role="navigation" class="nav-menu w-nav-menu">
                    <a href="#sobre" class="nav-link w-nav-link">Quem Somos</a>
<!--                    <a href="#" class="nav-link w-nav-link">Responsabilidade Social</a>-->
                    <a href="#sobre" class="nav-link w-nav-link">Nossa história</a>
                    <a href="#" class="nav-link w-nav-link" data-ix="open-nav-drop">Brinquedos</a>
                    <a href="<?php echo site_url(); ?>/oficina-da-roda" class="nav-link w-nav-link">Oficinas da Roda</a>
                    <a href="#contato" class="nav-link w-nav-link">Contato</a></nav>
                <div class="menu-btn w-nav-button" data-ix="close-drop-nav">
                    <div class="w-icon-nav-menu"></div>
                </div>
                <div class="box-icos-nav">
                    <ul class="ul-icos-nav w-clearfix w-list-unstyled">
                        <li class="li-icos-nav">
                            <a href="#" class="link-ico-nav w-inline-block" data-ix="open-search">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-lupa.png" alt="">
                            </a>
                        </li>
                        <li class="li-icos-nav">
                            <a href="https://api.whatsapp.com/send?phone=5511976943311" target="_blank" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-whats.png" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-nav">
        <div class="container-nav-prod w-container">
            <h2 class="h2-nav-prods">brinquedos</h2>
            <div class="row-nav-brinquedos w-row">
                <div class="col-nav-brinquedos w-col w-col-4 w-col-small-4 w-col-tiny-4">
                    <ul class="ul-nav-prods w-list-unstyled">
                        <li class="li-nav-prods">
                            <h3 class="heading" data-ix="close-drop-nav">Educativos</h3>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/artesanato" class="link-nav-prods">Artesanato</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/jogos" class="link-nav-prods">Jogos</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/fantasias" class="link-nav-prods">Fantasias</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/radicais" class="link-nav-prods">Radicais</a>
                        </li>
                    </ul>
                </div>
                <div class="col-nav-brinquedos w-col w-col-4 w-col-small-4 w-col-tiny-4">
                    <ul class="ul-nav-prods w-list-unstyled">
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <h3 class="heading">Atividades</h3>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/musicais" class="link-nav-prods">Musicais</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/cozinha" class="link-nav-prods">Cozinha</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/casinha" class="link-nav-prods">Casinha</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/outros" class="link-nav-prods">Outros</a>
                        </li>
                    </ul>
                </div>
                <div class="col-nav-brinquedos w-col w-col-4 w-col-small-4 w-col-tiny-4">
                    <ul class="ul-nav-prods w-list-unstyled">
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <h3 class="heading" data-ix="close-drop-nav">Por idade</h3>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/0-meses-a-1-ano" class="link-nav-prods">0 meses á 1 ano</a></li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/1-a-3-anos" class="link-nav-prods">1 á 3</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/3-a-5-anos" class="link-nav-prods">3 á 5</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/5-a-7-anos" class="link-nav-prods">5 á 7</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/7-a-9-anos" class="link-nav-prods">7 á 9</a>
                        </li>
                        <li class="li-nav-prods" data-ix="close-drop-nav">
                            <a href="<?php echo get_site_url();?>/categoria-produto/10-a-12-anos"" class="link-nav-prods">10 á 12</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-search">
        <div class="container-search">
            <div class="container-search">
                <form action="<?php bloginfo('url'); ?>/search.php" method="GET" class="box-search w-form">
                    <img src="<?php bloginfo('template_directory'); ?>/images/button.png" alt="" class="btn-fechar" data-ix="close-search">
                    <input type="text" value="<?php the_search_query();?>" class="search-input w-input" maxlength="256" id="s" name="s" placeholder="Pesquisar" required=""/>
                    <input type="text" name="post_type" id="post_type" value="produto" class="hidden" style="display:none;"/>
                    <input type="submit" id="searchbutton" value="Buscar" class="search-btn w-button"/>
                </form>
            </div>
        </div>

        </div>
    </div>

    </div>
</div>

