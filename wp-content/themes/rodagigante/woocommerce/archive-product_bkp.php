<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header();

global $product; //tem uma parada na pagina de listagem que nào tem aqui o qeu mano ?
?>
<?php

//echo $product->get_image_id();
//$product->get_gallery_image_ids(get_the_ID());
?>
<pre>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
    <div class="container w-container">
<!--        <div class="breadcrumb">-->
<!--            <span>Página principal</span>-->
<!--            <span>Tipos de Brincadeiras</span>-->
<!--        </div>-->

        </pre>



        <div class="menu-lateral">
            <h1>tipos<br>de<br>brincadeira</h1>
            <ul class="topics">
                <li><a href="">Brincando de céu aberto</a></li>
                <li><a href="">Brincando em dias chuvosos</a></li>
                <li><a href="">Brincando com os amigos</a></li>
                <li><a href="">Brincando com som</a></li>
                <li><a href="">Brincando de fazer arte</a></li>
                <li><a href="">Brincando com foto e concentração</a></li>
                <li><a href="">Brincando com equilibrio e coordenação</a></li>
                <li><a href="">Brincando com afeto</a></li>
                <li><a href="">Brincando com a natureza</a></li>
                <li><a href="">Brincando no banho</a></li>
            </ul>
        </div>


        <div class="area-listagem">
            <div class="produtos">
                <ul class="lista-produtos">
                    <?php
                    $args = array(
                        'post_type' => 'product',
                        'orderby'   => 'title',
                        'tax_query' => array(
                            array(
                                'taxonomy'  => 'product_cat',
                                'field'     => 'id',
                                'terms'     => 200
                            ),
                        ),
                        'posts_per_page' => -1
                    );
                    //var_dump($product); //nao ta rolando o debug?? faz tempo que eu não uso mas estava acho que tem que adicionar o projeto
                    $featured_query = new WP_Query( $args );
                    while ($featured_query->have_posts()) :
                        $featured_query->the_post();
                        $product = wc_get_product( $product->get_id() );
                        //echo $product->get_name();
                        $link_produto = get_permalink( $product->get_id() );

                        ?>

                        <li>
                            <a href="<?= $link_produto; ?>">
                                <?= $product->get_image(); ?>
                                <span class="nome-produto"><?= $product->get_name(); ?></span>
                                <span class="nome-produto"><?= $product->get_price(); ?></span>
                            </a>
                        </li>

                    <?php endwhile;
                    //var_dump($product); é o novo o projeto (interrogacao) rs é o normal novo era teste

                    wp_reset_query(); ?>
                    </ul>
                </div>
            </div>


<?php get_footer(); ?>




