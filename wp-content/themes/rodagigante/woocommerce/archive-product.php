<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
//do_action( 'woocommerce_before_main_content' );

?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	//do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php   
if ( woocommerce_product_loop() ) {

    /**
     * Hook: woocommerce_before_shop_loop.
     *
     * @hooked woocommerce_output_all_notices - 10
     * @hooked woocommerce_result_count - 20
     * @hooked woocommerce_catalog_ordering - 30
     */
    //do_action( 'woocommerce_before_shop_loop' );

    woocommerce_product_loop_start();

    if (wc_get_loop_prop('total')) {
        ?>
        <div class="section section-breadcrumb">
            <div class="container container-bread w-container">
                <ul class="ul-bread w-clearfix w-list-unstyled">
                    <li class="li-bread"><?php woocommerce_breadcrumb(['delimiter' => ' > ']); ?></li>

                </ul>
            </div>
        </div>


        <div class="area-listagem">
        <div class="produtos">
        <ul class="lista-produtos">
        <?php
        global $product;
        //foreach($lista as $item) { ?>


        <?php
        $category = get_queried_object();

        $args = array(
            'post_type' => 'product',
            'orderby' => 'title',
//            'limit' =>6,
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id',
                    'terms' => $category->term_id,

                ),
            ),
            //'posts_per_page' => 6,

        );


        $featured_query = new WP_Query($args);

        while ($featured_query->have_posts()) :
            $featured_query->the_post();
            $product = wc_get_product();
            //echo $product->get_name();
            $link_produto = get_permalink($product->get_id());

            //var_dump($args);
            ?>

            <li>
                <a href="<?= $link_produto; ?>">
                    <?= $product->get_image(); ?>
                    <span class="nome-produto"><?= $product->get_name(); ?></span>
                    <!-- <span class="nome-produto"><?= $product->get_price(); ?></span> -->
                </a>
            </li>

            <?php //echo count($link_produto);?>
        <?php endwhile;
        wp_reset_query();
        //var_dump($product);

        ?>
        </ul>
        </div>
        </div>
        <?php wordpress_pagination();
    }

}
?>

<?php
get_footer(  );

?>