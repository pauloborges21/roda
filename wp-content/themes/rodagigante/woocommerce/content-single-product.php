<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
//do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>

<div class="section section-breadcrumb">
    <div class="container container-bread w-container">
        <ul class="ul-bread w-clearfix w-list-unstyled">
            <li class="li-bread"><?php woocommerce_breadcrumb(['delimiter' => ' > ']); ?></li>

        </ul>
    </div>
</div>

<div class="section">
    <div class="container container-interna w-container">
        <div class="content-prod-det">
            <div class="row-det-prod w-row">
                <div class="col-img-prod w-col w-col-6">
                    <?php
                    global $product;
                    //echo $product->get_image_id();
                    //var_dump($product->get_gallery_image_ids(get_the_ID()));
                    ?>
                    <div class="box-img-det-prod">
                        <div data-duration-in="300" data-duration-out="100" class="prod-gallery w-tabs">
                            <div class="tb-content w-tab-content">
                                <div data-w-tab="Tab 1" class="foto-dstq-prod w-tab-pane w--tab-active">
                                    <div class="container-img-det-prod">
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-det-prod">
                                    </div>
                                </div>
                                <?php
                                $attachment_ids = $product->get_gallery_image_ids();
                                $i = 2;
                                foreach( $attachment_ids as $attachment_id ) {

                                    $image_link = wp_get_attachment_url( $attachment_id );
                                    ?>
                                    <div data-w-tab="Tab <?php echo $i;?>" class="foto-dstq-prod w-tab-pane">
                                        <div class="container-img-det-prod">

                                            <img src="<?php echo $image_link; ?>" alt="" class="img-det-prod">
                                        </div>
                                    </div>
                                    <?php

                                    //echo '<script>alert("'.$i.'");</script>';
                                    $i++;
                                }
                                ?>
                            </div>
                            <div class="prod-nav w-tab-menu">
                                <a data-w-tab="Tab 1" class="prod-item w-inline-block w-tab-link w--current">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="prev-img-det-prod">
                                </a>
                                <?php
                                $attachment_ids = $product->get_gallery_image_ids();
                                $i = 2;
                                foreach( $attachment_ids as $attachment_id ) {

                                    $image_link = wp_get_attachment_url( $attachment_id );
                                    ?>
                                    <a data-w-tab="Tab <?php echo $i; ?>" class="prod-item w-inline-block w-tab-link w--current">
                                        <img src="<?php echo $image_link; ?>" alt="" class="prev-img-det-prod">
                                    </a>
                                    <?php

                                    //echo '<script>alert("'.$i.'");</script>';
                                    $i++;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-desc-prod w-col w-col-6">
                    <h2 class="h2-prod"><?php the_title(); ?></h2>
                    <?php

                    //get the regular price of the product, but of a simple product
                    $product = wc_get_product();
                    ?>
                    <h3 class="h3-preco" style="display:none">R$ <?php echo $product->get_price(get_the_ID()); ?></h3>
                    <p class="desc-prod"><?php echo $product->get_description(); ?><br><br><?php echo $product->get_short_description(); ?>
                        <br><br>
                        <strong>Referência:</strong> <?php  echo $product->get_sku(); ?>
                        <br>
                        <strong>Material: </strong><?php $preenche_material = get_post_meta( get_the_id(), 'custom_text_field_title_material', true ); echo $preenche_material; ?>
                        <br>
                        <strong>Dimensões do produto: </strong><?php echo $product->get_length() . ' x ' . $product->get_width() . ' x ' . $product->get_height(); ?>
                        <br>
                        <strong>Dimensões da embalagem: </strong>
                        <?php
                        $preenche_comprimento = get_post_meta( get_the_id(), 'custom_text_field_title_caixa_comprimento', true );
                        $preenche_largura = get_post_meta( get_the_id(), 'custom_text_field_title_caixa_largura', true );
                        $preenche_altura = get_post_meta( get_the_id(), 'custom_text_field_title_caixa_altura', true );
                        echo $preenche_comprimento . ' x ' . $preenche_largura . ' x ' . $preenche_altura;
                        ?>
                        <br>
                        <strong>Fabricante: </strong><?php $preenche_fabricante = get_post_meta( get_the_id(), 'custom_text_field_title_fabricante', true ); echo $preenche_fabricante; ?>
                        <br>
                        <strong>INMETRO: </strong><?php $preenche_inmetro = get_post_meta( get_the_id(), 'custom_text_field_title_inmetro', true ); echo $preenche_inmetro; ?>
                        <br><br>
                        <!-- Roda Gigante Brinquedos Educativos, Sustentáveis e Feitos no Brasil. -->
                    <div style="width: 300px">
                        <a href="https://api.whatsapp.com/send?phone=5511976943311" target="_blank" class="add-cart w-button" style="width: 100%">Quer falar mais sobre os produtos? Nos chame no WhatsApp!</a>
                    </div>
                    </p>
                    <div class="form-frete w-form" style="display:none">
                        <form id="email-form" name="email-form" data-name="Email Form" class="w-clearfix">
                            <label for="cep-2" class="label-frete">Frete e Prazo</label>
                            <input type="text" class="input frete w-input" maxlength="256" name="cep" data-name="cep" placeholder="CEP" id="cep-2" required="">
                            <input type="submit" value="Calcular" data-wait="Aguarde" class="submit-frete w-button">
                        </form>
                        <div class="w-form-done">
                            <div>Thank you! Your submission has been received!</div>
                        </div>
                        <div class="w-form-fail">
                            <div>Oops! Something went wrong while submitting the form.</div>
                        </div>
                    </div>
                    <a href="?add-to-cart=<?php the_ID(); ?>" class="add-cart w-button" style="display:none">Adicionar no Carrinho • <!--R$ 299,00--></a>
                    <div class="row-prod-share w-row" style="display:none">
                        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                            <div class="w-embed"><a target="_blank" href="//www.facebook.com/sharer.php?u=http://roda-gigante.webflow.io/produto-interna" class="share__link share_face">
                                    <span class="share__text">Share</span>
                                </a></div>
                        </div>
                        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                            <div class="w-embed"><a target="_blank" href="//twitter.com/share?url=http://roda-gigante.webflow.io/produto-interna&amp;text=" class="share__link share_twitter">
                                    <span class="share__text">Tweet</span>
                                </a></div>
                        </div>
                        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                            <div class="w-embed"><a target="_blank" href="http://pinterest.com/pin/create/button/?url=http://roda-gigante.webflow.io/produto-interna&amp;media=http://cdn.shopify.com/s/files/1/0763/3377/products/MG_8248_1024x1024.jpg?v=1525841083&amp;description=" class="share__link share_pinterest">
                                    <span class="share__text">Pin</span>
                                </a></div>
                        </div>
                        <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                            <div class="w-embed"><a target="_blank" href="//plus.google.com/share?url=https://pampa.com.au/products/bath-and-meditate-bundle" class="share__link share_gplus">
                                    <span class="share__text">+1</span>
                                </a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-veja-tambem">
            <h2 class="h2-veja-tbm">veja também</h2>
            <div class="row-dstq-veja-tbm w-row">
                <div class="col-veja w-col w-col-3 w-col-medium-6">
                    <a href="#" class="link-veja w-inline-block">
                        <img src="images/img-brinquedo.png" srcset="images/img-brinquedo-p-500.png 500w, images/img-brinquedo.png 586w" sizes="(max-width: 479px) 100vw, 209.28125px" alt="" class="img-veja">
                        <div class="txt-veja">Woodbike 2 em 1</div>
                    </a>
                </div>
                <div class="col-veja w-col w-col-3 w-col-medium-6">
                    <a href="#" class="link-veja w-inline-block">
                        <img src="images/img-brinquedo12.png" alt="" class="img-veja">
                        <div class="txt-veja">Polvo</div>
                    </a>
                </div>
                <div class="col-veja w-col w-col-3 w-col-medium-6">
                    <a href="#" class="link-veja w-inline-block">
                        <img src="images/img-brinquedo13.png" alt="" class="img-veja">
                        <div class="txt-veja">Caixas de Encaixe</div>
                    </a>
                </div>
                <div class="col-veja w-col w-col-3 w-col-medium-6">
                    <a href="#" class="link-veja w-inline-block">
                        <img src="images/img-brinquedo2.png" alt="" class="img-veja">
                        <div class="txt-veja">Kit Frutas e Legumes</div>
                    </a>
                </div>
            </div>
        </div>
        <div class="content-news">
            <h2 class="h2-news">Receba novidades</h2>
            <div class="form-news w-form">
                <form id="email-form-2" name="email-form-2" data-name="Email Form 2" class="w-clearfix">
                    <input type="text" class="input news w-input" maxlength="256" name="name" data-name="Name" placeholder="Assine nossa Newsletter" id="name">
                    <input type="submit" value="enviar" data-wait="Aguarde..." class="submit-news w-button">
                </form>
                <div class="w-form-done">
                    <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                    <div>Oops! Something went wrong while submitting the form.</div>
                </div>
            </div>
        </div>
    </div>
</div>



<!--<div id="product-<?php /*the_ID(); */?>" <?php /*wc_product_class(); */?>>

	<?php
/*		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
//do_action( 'woocommerce_before_single_product_summary' );
?>

	<div class="summary entry-summary">
		<?php
/*			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
//do_action( 'woocommerce_single_product_summary' );
?>
	</div>

	<?php
/*		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
//do_action( 'woocommerce_after_single_product_summary' );
?>
</div>-->

<?php //do_action( 'woocommerce_after_single_product' ); ?>
