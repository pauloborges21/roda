<?php

get_header()
?>

    <style>
        .breadcrumb {
            font-size: 12px;
            font-family: "Myriad-Pro", Arial, sans-serif;
            color: #7c7c7c;
            width: 100%;
            display: block;
            padding: 115px 0 0 177px;
        }

        .breadcrumb span {
            position: relative;
            margin-right: 18px;
        }

        .breadcrumb span:after {
            content: ">";
            display: inline-block;
            position: absolute;
            font-size: 13px;
            padding-left: 8px;
            margin-right: 8px;
        }

        .breadcrumb span:last-of-type:after {
            display: none;
        }

        .menu-lateral h1{
            font-weight: 500;
        }
        .menu-lateral {
            font-size: 26px;
            font-family: Champagne & limousines' , sans-serif;
            color: #7c7c7c;
            width: 30%;
            font-style: bold;
            display: flex-start;
            float: left;
            padding: 30px 0 0 177px;
        }

        .topics {
            font-size: 12px;
            font-family: "Myriad-Pro", Arial, sans-serif;
            color: #7c7c7c;
            padding-left: 5px;
        }

        .area-listagem {
            display: flex;
        }

        .filter {
            text-align: right;
            padding: 16px 40px 30px 0;
            font-family: "Myriad-Pro", Arial, sans-serif;
            color: #7c7c7c;
        }

        .filter form {
            display: flex;
            justify-content: flex-end;
        }

        .filter label {
            margin-left: 10px;
            font-weight: 500;
        }

        .produtos {
            width: 100%;
        }

        .topics {
            list-style-type: none;
        }

        .topics a {
            text-decoration: none;
            color: inherit;
            padding: 10px 0;
            font-size: "Montserrat", sans-serif;
            font-weight: 100;
            letter-spacing: 0.1rem;
            line-height: 1.4;
            display: block;
            width: 100%;
        }
        .lista-produtos{
            list-style: none;
            display: flex;
            justify-content: space-between;
            flex-wrap: wrap;
        }
        .lista-produtos li{
            width: 30%;
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
            margin-bottom: 40px;
            text-transform: uppercase;
            letter-spacing: 0.5px;
        }
        .nome-produto{
            padding: 8px 0;
            display: block;
            width: 100%;
            font-weight: bold;
            text-align: center;
        }
    </style>




<?php if ( have_posts('products') ) : while ( have_posts('products') ) : the_post('products'); ?>

    <?php
    /*$produtos_get = wc_get_products([

        'limit' => 6,
        'orderby' => 'rand',
//'tag' => ['brincando-com-foto-e-concentracao'],


    ]);*/


// function format_produtos($produtos){

//     $produtos_final =[];

//     foreach($produtos as $item){
//         $produtos_final[] = [
//             'name' => $item->get_name(),

//             'preco' => $item->get_price_html(),
//             'link' => $item->get_permalink( ),
//             'img' => wp_get_attachment_image_src($item->get_image_id()),



//             //'price' => ,

//         ];

//     }
//     return $produtos_final;
// }

// $lista = format_produtos($produtos_get);
// print_r($lista);

    ?>



    <div class="container w-container">
        <div class="breadcrumb">
            <span>Página principal</span>
            <span>Tipos de Brincadeiras</span>
        </div>
        <div class="menu-lateral">
            <h1>tipos<br>de<br>brincadeira</h1>
            <ul class="topics">
                <li><a href="">Brincando de céu aberto</a></li>
                <li><a href="">Brincando em dias chuvosos</a></li>
                <li><a href="">Brincando com os amigos</a></li>
                <li><a href="">Brincando com som</a></li>
                <li><a href="">Brincando de fazer arte</a></li>
                <li><a href="">Brincando com foto e concentração</a></li>
                <li><a href="">Brincando com equilibrio e coordenação</a></li>
                <li><a href="">Brincando com afeto</a></li>
                <li><a href="">Brincando com a natureza</a></li>
                <li><a href="">Brincando no banho</a></li>
            </ul>
        </div>
        <!--
            <div class="filter">
                <form action="/">
                    <span>Organizar por:</span>
                    <label> rolou ? pera que deu loop infinito..hahahaha
                        Menor preço
                        <input type="radio" name="filter">
                    </label>
                    <label>
                        Mais Vendidos
                        <input type="radio" name="filter">
                    </label>
                    <label>
                        Melhor Nota
                        <input type="radio" name="filter">
                    </label>
                </form>
            </div> -->


        <div class="area-listagem">
            <div class="produtos">
                <ul class="lista-produtos">
                    <?php
                    //foreach($lista as $item) { ?>


                    <?php
/*
                    $category = get_queried_object();
echo '<script>alert("'. $category->term_id .'");</script>';
                    $args = array(
                        'post_type' => 'product',
                        'orderby'   => 'title',
                        'posts_per_page' => -1,
                        'category' => $category->term_id
                    );
                    global $product;
                    $featured_query = new WP_Query( $args );
                    while ($featured_query->have_posts()) :
                        $featured_query->the_post();
                        $product = wc_get_product( $product->get_id() );
                        //var_dump($product);
                        //echo $product->get_name();
                        $link_produto = get_permalink( $product->get_id() );

                        ?>

                        <li>
                            <a href="<?= $link_produto; ?>">
                                <?= $product->get_image(); ?>
                                <span class="nome-produto"><?= $product->get_name(); ?></span>
                                <span class="nome-produto"><?= $product->get_price(); ?></span>
                            </a>
                        </li>

                    <?php endwhile;
                    //var_dump($product);
                    wp_reset_query();*/ ?>

                    <!-- <li>
                    <a href="<?php //$lista['link']; ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/polvo.png" alt="polvo">
                    <span class="nome-produto"><?php // $item['name']; ?></span>
                    <span class="nome-produto"><?php // $item['preco']; ?></span>
                    </a>

                </li>

                <li>
                    <img src="<?php bloginfo('template_directory'); ?>/images/leao.png" alt="leao">
                    <span class="nome-produto">leão</span>
                </li>
                <li>
                    <img src="<?php bloginfo('template_directory'); ?>/images/camera.png" alt="camera">
                    <span class="nome-produto">câmera fotográfica</span>
                </li>
                <li>
                    <img src="<?php bloginfo('template_directory'); ?>/images/sorvete.png" alt="picole">
                    <span class="nome-produto">picolé</span>
                </li>
                <li>
                    <img src="<?php bloginfo('template_directory'); ?>/images/smile.png" alt="boneco">
                    <span class="nome-produto">boneco gutinho</span>
                </li>
                <li>
                    <img src="<?php bloginfo('template_directory'); ?>/images/carro.png" alt="bondinho">
                    <span class="nome-produto">bondinho desmonte e encaixe</span>
                </li> -->
                </ul>
            </div>
        </div>
    </div>
<?php endwhile; else: ?>

<?php endif; ?>

<?php get_footer()?>