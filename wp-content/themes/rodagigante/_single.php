<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap"><?php  /* Template Name: Busca de produtos  */ 
	$caminhoTema = esc_url(get_template_directory_uri());
	$id = get_the_ID();
 	$consulta = $_REQUEST['query'];
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js home">
	
	<head>
		<title>Resultado de busca</title>
		<meta content="Resultado de busca" property="og:title">
		
		 <style type="text/css"> @media screen and (min-width: 1200px){.w-container{max-width: 1240px;}}select{-webkit-appearance: none !important; /*Removes default chrome and safari style*/ -moz-appearance: none !important; /*Removes default style Firefox*/ -ms-appearance: none; -o-appearance:none !important; appearance:none !important; overflow:hidden; width: 120%;}</style>
	
		<?php get_header();?>
		
		<!-- Início Conteúdo Interna-->
		<?php
			$titulo = get_the_title ($id);
			$linkPagina = get_permalink($id);
		?>

		<div class="section section-bread">
			<div class="container container-bread w-container"><a href="<?php echo get_site_url();?>" class="link-bread">Home</a>	<img src="<?php echo $caminhoTema;?>/images/icon-seta-menu.png" alt="" class="img-divisor-bread"><a href="<?php echo $linkPagina; ?>" class="link-bread"><?php echo $titulo; ?><br></a>
			</div>
			<img src="<?php echo $caminhoTema;?>/images/bg-bread1.png" alt="" class="image-2">
		</div>
			
	<div class="section sec-lista-produtos">
        <div class="container cont-list-prods w-container">
           <div class="item-lista-prods-all w-clearfix">
            
              <h1>Resultados da busca</h1> 
		 
              <!--<p>Exibibindo resultados para a consulta "<strong></strong>"</p> -->
              
              <div class="separadorProdutos"></div>
              
               <?php  
					 
	             // Início Repetidor de Produtos - Busca por cateoria
					
				$termIds = get_terms([
				  'name__like' => $consulta,
				  'fields' => 'ids'
					]);  //Consulta os id  pelo valor digitado
	
			   
				  //Busca Pelos ids encontrados
				  
				  $args = array('post_type' => 'product', 'posts_per_page' =>'-1',
    'post_status' => 'publish');
				  
				  $args['tax_query']=array(
					array(
						'taxonomy' => 'product_cat',
						'field' => 'name',
					  	'field' => 'id',
            			'terms' => $termIds,
					)
				);
					    
					$loop = new WP_Query( $args );
			   
			        $countCat = $loop->post_count;
			  
					if ( $loop->have_posts() ) {
						while ( $loop->have_posts() ) : $loop->the_post();
						global $product;
						
						$id = $product->get_id();
						$thumb = get_field("imagem_thumbnail",$id);
						
						if ($thumb==""){
							$thumb =  esc_url(get_template_directory_uri())."/images/default-prod.jpg";
						}
						
						$link = get_permalink($id);
						$cod = get_field("codigo",$id); //Código do Produto
						
						$terms = get_the_terms ( $id, 'product_cat' );
						foreach ( $terms as $term ) {
							 $cat_name = $term->name;
							
						}
					?>	
             
             			<div class="item-lista-prods">
        <div class="img-prod-lista"><img src="<?php echo $thumb; ?>" alt="<?php echo the_title();?>"></div>
        <div class="txt-explica-prod"><?php echo the_title();?></div>
        <div class="txt-codigo-protduto2">Cód.<?php echo $cod;?></div><a target="_blank" href="<?php echo $link;?>" class="bt-prod-lista w-button">saiba mais</a></div>
             
             <?php	
						endwhile;
					} 
					wp_reset_postdata();
				
				  ?>
            
            <?php
              //Início Repetidor de Produtos - Busca por nome
            $args = array('post_type' => 'product', 'posts_per_page' =>'-1',
    'post_status' => 'publish','s'=>$consulta,'post__not_in' =>$termIds,
        'tax_query'            => array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'term_id', // Or 'name' or 'term_id'
                'terms'    => array(15),
                'operator' => 'NOT IN', // Excluded
            )
        ));
			   
			 	$loop = new WP_Query( $args );
			   
			   $prodCat = $loop->post_count;
 
					if ( $loop->have_posts() ) {
						while ( $loop->have_posts() ) : $loop->the_post();
						global $product;
						
						$id = $product->get_id();
						$thumb = get_field("imagem_thumbnail",$id);
						
						if ($thumb==""){
							$thumb =  esc_url(get_template_directory_uri())."/images/default-prod.jpg";
						}
						
						$link = get_permalink($id);
						$cod = get_field("codigo",$id); //Código do Produto
						
						$terms = get_the_terms ( $id, 'product_cat' );
						foreach ( $terms as $term ) {
							 $cat_name = $term->name;
						
						}
					?>	
             
            			<div class="item-lista-prods">
        <div class="img-prod-lista"><img src="<?php echo $thumb; ?>" alt="<?php echo the_title();?>"></div>
        <div class="txt-explica-prod"><?php echo the_title();?></div>
        <div class="txt-codigo-protduto2">Cód.<?php echo $cod;?></div><a target="_blank" href="<?php echo $link;?>" class="bt-prod-lista w-button">saiba mais</a></div>
            			
        <?php	
						endwhile;
					} 
					wp_reset_postdata();
				
				  ?>
            
            
            <?php 
			   //if($prodCat==0 && $countCat==0){
			 ?> 
            	<!--<p>Nenhum resultado encontrado para a sua consulta.</p>-->
            	
            <?php 
			   //}
			 ?> 






<?php
              //Início Repetidor de Produtos - Busca por nome
     $args = array('post_type' => 'product', 
	'posts_per_page' =>'-1',
   	 'post_status' => 'publish',
	'meta_key'		=> 'codigo',
				
        'meta_value'	=> $consulta,
	'post__not_in' =>$termIds,
        'tax_query'            => array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'term_id', // Or 'name' or 'term_id'
                'terms'    => array(15),
                'operator' => 'NOT IN', // Excluded
            )
        ));
			   
			 	$loop = new WP_Query( $args );
			   
			   $CodigoCount = $loop->post_count;
 
					if ( $loop->have_posts() ) {
						while ( $loop->have_posts() ) : $loop->the_post();
						global $product;
						
						$id = $product->get_id();
						$thumb = get_field("imagem_thumbnail",$id);
						
						if ($thumb==""){
							$thumb =  esc_url(get_template_directory_uri())."/images/default-prod.jpg";
						}
						
						$link = get_permalink($id);
						$cod = get_field("codigo",$id); //Código do Produto
						
						$terms = get_the_terms ( $id, 'product_cat' );
						foreach ( $terms as $term ) {
							 $cat_name = $term->name;
						
						}
					?>	
             
            			<div class="item-lista-prods">
        <div class="img-prod-lista"><img src="<?php echo $thumb; ?>" alt="<?php echo the_title();?>"></div>
        <div class="txt-explica-prod"><?php echo the_title();?></div>
        <div class="txt-codigo-protduto2">Cód.<?php echo $cod;?></div><a target="_blank" href="<?php echo $link;?>" class="bt-prod-lista w-button">saiba mais</a></div>
            			
        <?php	
						endwhile;
					} 
					wp_reset_postdata();
				
				  ?>

				  <?php 
			   if($prodCat==0 && $countCat==0 && $CodigoCount ==0){
			 ?> 
            	<p>Nenhum resultado encontrado para a sua consulta.</p>
            	
            <?php 
			   }
			 ?>
            









              
			</div>
		</div>
     </div>	 
				

		  <!--- Fim conteúdo interna--> 
		 
	<?php get_footer();?>
	
	<!-- Mostra e Esconde senhas--> 
	<script> 
		function ValidaForm(){
			if (('#email-5').val()=""){
				alert("Digite um email"); 
				return false;
			}else{
				return true;
			}
		}
	</script>
</html>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/post/content', get_post_format() );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

				the_post_navigation( array(
					'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
				) );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
