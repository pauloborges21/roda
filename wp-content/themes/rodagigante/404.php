<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>



<div class="section section-404">
    <div class="container container-404 w-container">
        <div class="titu-404">Algo deu errado...</div>
        <div>Erro 404</div><img src="<?php bloginfo('template_directory'); ?>/images/bike-404.png" alt="" class="bike-404">
        <div class="footer-404"><img src="<?php bloginfo('template_directory'); ?>/images/logo-footer.png" alt="">
            <ul class="ul-redes w-clearfix w-list-unstyled">
                <li class="li-redes"><a href="https://api.whatsapp.com/send?phone=5511976943311" target="_blank" class="w-inline-block"><img src="<?php bloginfo('template_directory'); ?>/images/icon-dourado-whats.png" alt=""></a></li>
                <li class="li-redes"><a href="https://www.facebook.com/rodagigantebrinquedos" target="_blank" class="w-inline-block"><img src="<?php bloginfo('template_directory'); ?>/images/icon-dourado-face.png" alt=""></a></li>
                <li class="li-redes"><a href="https://www.instagram.com/rodagigantebrinquedos/" target="_blank" class="w-inline-block"><img src="<?php bloginfo('template_directory'); ?>/images/icon-dourado-insta.png" alt=""></a></li>
            </ul>
            <div class="txt-copyright">Roda gigante brinquedos • Copyright © Todos os direitos reservados.</div>
        </div>
    </div>
</div>
<script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/webflow.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/webflow-2.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js" type="text/javascript"></script>
<!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>