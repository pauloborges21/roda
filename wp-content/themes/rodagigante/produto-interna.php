<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Fri Sep 21 2018 15:09:34 GMT+0000 (UTC)  -->
<html data-wf-page="5b9c123a1430c32ca430d96a" data-wf-site="5b9a7a40c03054f6cc545866">
<head>
  <meta charset="utf-8">
  <title>Produto interna</title>
  <meta content="Produto interna" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/roda-gigante.webflow_old2.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.png" rel="apple-touch-icon">
  <style type="text/css">
      @media screen and (min-width: 1280px) {
        .w-container {
          max-width: 1640px;
        }
        .nav-menu {
          font-size: 18px;
        }
        .nav-link {
          margin-right: 40px;
          margin-left: 40px;
        }
        .li-icos-nav {
          margin-right: 20px;
          margin-left: 20px;
          font-size: 13px;
          font-weight: 600;
        }
        .container.container-main-banner {
          background-position: 50% 65%;
        }
        .logo-main-banner {
          width: 450px;
        }
        .box-institu {
          height: 450px;
        }
        h2 {
          font-size: 25px;
    	  line-height: 45px;
          font-weight: 500;
          color: #707070;
        }
        .content-institu {
          padding-top: 180px;
          font-size: 15px;
    	  line-height: 20px;
        }
        .h2-sustentabilidade {
          letter-spacing: 10px;
        }
        .txt-slide {
           font-size: 15px;
           line-height: 17px;
        }
        .box-dstq-left, .box-dstq-right {
          height: 730px;
        }
        .dstq-g {
          height: 450px;
          background-position: 50% 50%;
    	  background-size: auto 350px;
        }
        .img-dstq {
          max-width: 100%;
          width: 70%;
        }
        .hover-nome-prod {
          font-size: 20px;
          line-height: 40px;
        }
        .dstq-p {
          height: 280px;
        }
        .dstq-g2 {
          height: 380px;
        }
        .dstq-p2 {
          height: 350px;
        }
        .content-prod-det {
          width: 55%;
      	}
}
 select {
    -webkit-appearance: none !important; /*Removes default chrome and safari style*/
    -moz-appearance: none !important; /*Removes default style Firefox*/
    -ms-appearance: none;
    -o-appearance:none !important;
    appearance:none !important;
    overflow:hidden;
    width: 120%;
  }
  select::-ms-expand {
    display: none !important;
}
  .select-cadastro::-ms-expand {
    display: none !important;
}
</style>
</head>
<body>
  <div class="section section-header-interna">
    <div class="container"></div>
    <div class="container w-container">
      <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
        <div class="container-nav container-nav-interna w-container"><img src="images/logo-footer.png" class="logo-nav-interna">
          <nav role="navigation" class="nav-menu w-nav-menu"><a href="#" class="nav-link w-nav-link">Quem Somos</a><a href="#" class="nav-link w-nav-link">Responsabilidade Social</a><a href="#" class="nav-link w-nav-link">Nossa história</a><a href="#" class="nav-link w-nav-link">Brinquedos</a></nav>
          <div class="menu-btn w-nav-button">
            <div class="w-icon-nav-menu"></div>
          </div>
          <div class="box-icos-nav">
            <ul class="ul-icos-nav w-clearfix w-list-unstyled">
              <li class="li-icos-nav"><a href="#" class="link-ico-nav w-inline-block"><img src="images/ico-lupa.png"><div>pesquisar</div></a></li>
              <li class="li-icos-nav"><a href="#" class="link-ico-nav w-inline-block"><img src="images/ico-login.png"><div>login</div></a></li>
              <li class="li-icos-nav"><a href="#" class="link-ico-nav w-inline-block"><img src="images/ico-pedidos.png"><div>pedidos</div></a></li>
              <li class="li-icos-nav"><a href="#" class="link-ico-nav w-inline-block"><img src="images/ico-carrinho.png"><div>carrinho</div><div class="qtd-carrinho"><div>10</div></div></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section section-breadcrumb">
    <div class="container container-bread w-container">
      <ul class="ul-bread w-clearfix w-list-unstyled">
        <li class="li-bread"><a href="#" class="link-bread">Página Principal</a></li>
        <li class="li-bread">
          <div>&gt;</div>
        </li>
        <li class="li-bread"><a href="#" class="link-bread">Home</a></li>
        <li class="li-bread">
          <div>&gt;</div>
        </li>
        <li class="li-bread"><a href="#" class="link-bread">Carneiro de Balanço</a></li>
      </ul>
    </div>
  </div>
  <div class="section">
    <div class="container container-interna w-container">
      <div class="content-prod-det">
        <div class="row-det-prod w-row">
          <div class="col-img-prod w-col w-col-6">
            <div class="box-img-det-prod">
              <div data-duration-in="300" data-duration-out="100" class="prod-gallery w-tabs">
                <div class="tb-content w-tab-content">
                  <div data-w-tab="Tab 1" class="foto-dstq-prod w-tab-pane">
                    <div class="container-img-det-prod"><img src="images/img-brinquedo4.png" class="img-det-prod"></div>
                  </div>
                  <div data-w-tab="Tab 2" class="foto-dstq-prod w-tab-pane w--tab-active">
                    <div class="container-img-det-prod"><img src="images/carneiro2.png" class="img-det-prod"></div>
                  </div>
                  <div data-w-tab="Tab 3" class="foto-dstq-prod w-tab-pane">
                    <div class="container-img-det-prod"><img src="images/img-brinquedo4.png" class="img-det-prod"></div>
                  </div>
                  <div data-w-tab="Tab 4" class="foto-dstq-prod w-tab-pane">
                    <div class="container-img-det-prod"><img src="images/carneiro2.png" class="img-det-prod"></div>
                  </div>
                </div>
                <div class="prod-nav w-tab-menu"><a data-w-tab="Tab 1" class="prod-item w-inline-block w-tab-link"><img src="images/img-brinquedo4.png" class="prev-img-det-prod"></a><a data-w-tab="Tab 2" class="prod-item w-inline-block w-tab-link w--current"><img src="images/carneiro2.png" class="prev-img-det-prod"></a><a data-w-tab="Tab 3" class="prod-item w-inline-block w-tab-link"><img src="images/img-brinquedo4.png" class="prev-img-det-prod"></a><a data-w-tab="Tab 4" class="prod-item w-inline-block w-tab-link"><img src="images/carneiro2.png" class="prev-img-det-prod"></a></div>
              </div>
            </div>
          </div>
          <div class="col-desc-prod w-col w-col-6">
            <h2 class="h2-prod">CARNEIRO DE BALANÇO</h2>
            <h3 class="h3-preco">r$ 299,00</h3>
            <p class="desc-prod">Para a criançada soltar a criatividade e treinar o equilíbrio. O carneiro de balanço é feito de madeira maciça (pinus) e pelúcia imitando a lã do carneiro! Além de brinquedo, ele ainda funciona como objeto de decoração :)<br><br><strong>Referência:</strong> RG0000 <br><strong>Material: </strong>Pinus e pelúcia<br><strong>Dimensões do produto: </strong>77 cm x 60 cm x 23 cm<br><strong>Dimensões da embalagem: </strong>50 cm x 45 cm x 80 cm<br><strong>Fabricante: </strong>Bohney<br><strong>INMETRO: </strong>Recomendável para crianças maiores de 1 ano<br><br><!--Roda Gigante Brinquedos Educativos, Sustentáveis e Feitos no Brasil.--></p>
            <div class="form-frete w-form">
              <form id="email-form" name="email-form" data-name="Email Form" class="w-clearfix"><label for="cep-2" class="label-frete">Frete e Prazo</label><input type="text" class="input frete w-input" maxlength="256" name="cep" data-name="cep" placeholder="CEP" id="cep-2" required=""><input type="submit" value="Calcular" data-wait="Aguarde" class="submit-frete w-button"></form>
              <div class="w-form-done">
                <div>Thank you! Your submission has been received!</div>
              </div>
              <div class="w-form-fail">
                <div>Oops! Something went wrong while submitting the form.</div>
              </div>
            </div><a href="#" class="add-cart w-button">Adicionar no Carrinho • R$ 299,00</a>
            <div class="row-prod-share w-row">
              <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                <div class="w-embed"><a target="_blank" href="//www.facebook.com/sharer.php?u=http://roda-gigante.webflow.io/produto-interna" class="share__link share_face">
      <span class="share__text">Share</span>
</a></div>
              </div>
              <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                <div class="w-embed"><a target="_blank" href="//twitter.com/share?url=http://roda-gigante.webflow.io/produto-interna&amp;text=" class="share__link share_twitter">
                  <span class="share__text">Tweet</span>
                </a></div>
              </div>
              <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                <div class="w-embed"><a target="_blank" href="http://pinterest.com/pin/create/button/?url=http://roda-gigante.webflow.io/produto-interna&amp;media=http://cdn.shopify.com/s/files/1/0763/3377/products/MG_8248_1024x1024.jpg?v=1525841083&amp;description=" class="share__link share_pinterest">
                  <span class="share__text">Pin</span>
                </a></div>
              </div>
              <div class="w-col w-col-3 w-col-small-3 w-col-tiny-3">
                <div class="w-embed"><a target="_blank" href="//plus.google.com/share?url=https://pampa.com.au/products/bath-and-meditate-bundle" class="share__link share_gplus">
                  <span class="share__text">+1</span>
                </a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="content-veja-tambem">
        <h2 class="h2-veja-tbm">veja também</h2>
        <div class="row-dstq-veja-tbm w-row">
          <div class="col-veja w-col w-col-3 w-col-medium-6"><a href="#" class="link-veja w-inline-block"><img src="images/img-brinquedo.png" srcset="images/img-brinquedo-p-500.png 500w, images/img-brinquedo.png 586w" sizes="(max-width: 479px) 100vw, 209.28125px" class="img-veja"><div class="txt-veja">Woodbike 2 em 1</div></a></div>
          <div class="col-veja w-col w-col-3 w-col-medium-6"><a href="#" class="link-veja w-inline-block"><img src="images/img-brinquedo12.png" class="img-veja"><div class="txt-veja">Polvo</div></a></div>
          <div class="col-veja w-col w-col-3 w-col-medium-6"><a href="#" class="link-veja w-inline-block"><img src="images/img-brinquedo13.png" class="img-veja"><div class="txt-veja">Caixas de Encaixe</div></a></div>
          <div class="col-veja w-col w-col-3 w-col-medium-6"><a href="#" class="link-veja w-inline-block"><img src="images/img-brinquedo2.png" class="img-veja"><div class="txt-veja">Kit Frutas e Legumes</div></a></div>
        </div>
      </div>
      <div class="content-news">
        <h2 class="h2-news">Receba novidades</h2>
        <div class="form-news w-form">
          <form id="email-form-2" name="email-form-2" data-name="Email Form 2" class="w-clearfix"><input type="text" class="input news w-input" maxlength="256" name="name" data-name="Name" placeholder="Assine nossa Newsletter" id="name"><input type="submit" value="enviar" data-wait="Aguarde..." class="submit-news w-button"></form>
          <div class="w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail">
            <div>Oops! Something went wrong while submitting the form.</div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div>
    <div class="container container-footer w-container"><img src="images/logo-frase.png" class="logo-footer">
      <div class="box-social">
        <div class="row-social w-row">
          <div class="w-col w-col-6"><a href="https://www.facebook.com/rodagigantebrinquedos" target="_blank" class="a-social w-inline-block"><img src="images/ico-face2.png"></a></div>
          <div class="w-col w-col-6"><a href="https://www.instagram.com/rodagigantebrinquedos/" target="_blank" class="a-social w-inline-block"><img src="images/ico-insta.png"></a></div>
        </div>
      </div>
      <div>Roda gigante brinquedos • Copyright © Todos os direitos reservados.</div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>