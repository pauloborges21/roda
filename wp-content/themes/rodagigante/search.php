<?php
/*
Template Name: Search Page
*/

$caminhoTema = esc_url(get_template_directory_uri());
$id = get_the_ID();
$consulta = $_GET['s'];
?>

<?php get_header(); ?>

<!-- Início Conteúdo Interna-->
<!--		--><?php
//			$titulo = get_the_title ($id);
//			$linkPagina = get_permalink($id);
//

?>

<div class="section section-breadcrumb">
    <div class="container container-bread w-container">
        <ul class="ul-bread w-clearfix w-list-unstyled">
            <li class="li-bread">
                <?php woocommerce_breadcrumb(['delimiter' => ' > ']); ?></li>

        </ul>
    </div>
</div>

<div class="section sec-lista-produtos">
    <div class="container cont-list-prods w-container">
        <div class="item-lista-prods-all w-clearfix">

            <h1>Resultados da busca</h1>

            <!--<p>Exibibindo resultados para a consulta "<strong></strong>"</p> -->

            <div class="separadorProdutos"></div>

            <?php

            global $product;
            // print_r($consulta) ;
            // Início Repetidor de Produtos - Busca por categoria

            $termIds = get_terms([
                'name__like' => $consulta,
                'fields' => 'ids',

            ]);  //Consulta os id  pelo valor digitado


            //Busca Pelos ids encontrados

            $args = array('post_type' => 'product',
                //'posts_per_page' => '6',
                'post_status' => 'publish',


            );

            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'name',
                    'field' => 'id',
                    'terms' => $termIds,



                )
            );

            $loop = new WP_Query($args);

            $countCat = $loop->post_count;


            if ($loop->have_posts()) {
            while ($loop->have_posts()) :
            $loop->the_post();
            global $product;

            $id = $product->get_id();
            $thumb = the_post_thumbnail_url();

           // print_r($product)  ;

            if ($thumb == "") {
                $thumb = esc_url(get_template_directory_uri()) . "/images/default-prod.jpg";
            }

            $link = get_permalink($id);
            //$cod = get_field("codigo",$id); //Código do Produto

            $terms = get_the_terms($id, 'product_cat');
            foreach ($terms as $term) {
                $cat_name = $term->name;

            }
            ?>


            <div class="area-listagem" >
                <div class="produtos">
                    <ul class="lista-produtos">

                        <li>
                            <a href="<?= $link ?>">
                                <img src="<?php the_post_thumbnail_url(); ?>">
                                   <span class=" nome-produto"><?php echo the_title(); ?></span>
                                <!-- <span class="nome-produto"><?php// $item['preco']; ?></span> -->
                            </a>
                        </li>

                    </ul>

                    <?php
                    endwhile;
                    }
                    wp_reset_postdata();

                    ?>

                    <?php
                    //Início Repetidor de Produtos - Busca por nome
                    $args = array('post_type' => 'product',
                        'posts_per_page' => '6',
                        'post_status' => 'publish',
                        's' => $consulta,
                        'post__not_in' => $termIds,

                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_cat',
                                'field' => 'term_id', // Or 'name' or 'term_id'
                                'terms' => array(15),
                                'operator' => 'NOT IN', // Excluded

                            )
                        ));

                    $loop = new WP_Query($args);

                    $prodCat = $loop->post_count;

                    if ($loop->have_posts()) {
                    while ($loop->have_posts()) :
                    $loop->the_post();
                    global $product;

                    $id = $product->get_id();
                    $thumb = the_post_thumbnail_url();

                    if ($thumb == "") {
                        $thumb = esc_url(get_template_directory_uri()) . "/images/default-prod.jpg";
                    }

                    $link = get_permalink($id);
                    //$cod = get_field("codigo",$id); //Código do Produto

                    $terms = get_the_terms($id, 'product_cat');
                    foreach ($terms as $term) {
                        $cat_name = $term->name;

                    }
                    ?>


                    <div class="area-listagem">
                        <div class="produtos">
                            <ul class="lista-produtos">

                                <li>
                                    <a href="<?= $link ?>">
                                        <img src="<?php the_post_thumbnail_url(); ?>">
                                   <span class=" nome-produto"><?php echo the_title(); ?></span>
                                        <!-- <span class="nome-produto"><?php// $item['preco']; ?></span> -->
                                    </a>
                                </li>

                            </ul>
                            <?php
                            endwhile;
                            }
                            wp_reset_postdata();

                            ?>


                            <?php
                            if ($prodCat == 0 && $countCat == 0) {
                                ?>
                                <p>Nenhum resultado encontrado para a sua consulta.</p>

                                <?php
                            }
                            ?>


                        </div>
                    </div>
                    <?php get_the_posts_pagination()?>
                </div>
            </div>

                <!--- Fim conteúdo interna-->

        <?php get_footer(); ?>
