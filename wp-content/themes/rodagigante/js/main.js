/* Máscaras ER */
function mascara(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara()", 1)
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value)
}

function mtel(v) {
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

/*Função que padroniza CEP*/
function mcep(v){
    v=v.replace(/D/g,""); //Remove tudo o que não é dígito     
    v=v.replace(/^(\d{5})(\d)/g,"$1-$2"); //Coloca hífen entre o quinto e o sexto dígitos
    return v;
}



// Formulario

$('.formphp').on('submit', function () {
  var emailContato = ""; // Escreva aqui o seu e-mail

  var that = $(this),
      url = that.attr('action'),
      type = that.attr('method'),
      data = {};

  that.find('[name]').each(function (index, value) {
      var that = $(this),
          name = that.attr('name'),
          value = that.val();

      data[name] = value;
  });

  $.ajax({
      url: url,
      type: type,
      data: data,
      success: function (response) {

          if ($('[name="leaveblank"]').val().length != 0) {
              $('.formphp').html("<div id='form-erro'></div>");
              $('#form-erro').html("<span>Falha no envio!</span><p>Você pode tentar novamente, ou enviar direto para o e-mail " + emailContato + " </p>")
                  .hide()
                  .fadeIn(1000, function () {
                      $('#form-erro');
                  });
          } else {

              $('.formphp').html("<div id='form-send'></div>");
              $('#form-send').html("<span>Mensagem enviada!</span><p>Obrigado pelo contato.</p>")
                  .hide()
                  .fadeIn(1000, function () {
                      $('#form-send');
                  });
          };
      },
      error: function (response) {
          $('.formphp').html("<div id='form-erro'></div>");
          $('#form-erro').html("<span>Falha no envio!</span><p>Você pode tentar novamente, ou enviar direto para o e-mail " + emailContato + " </p>")
              .hide()
              .fadeIn(1000, function () {
                  $('#form-erro');
              });
      }
  });

  return false;
});





