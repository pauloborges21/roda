

<?php
//Template Name: Home
get_header();
?>

    <!-- <div class="container container-main-banner w-container"><img src="<?php //bloginfo('template_directory'); ?>/images/logo_animado.gif" alt="" class="logo-main-banner"></div> -->

<?php
//							$args = array(
//
//								'hide_empty' => false,
//								'parent' => 0,
//								'orderby' => 'title',
//								'order' => 'ASC',
//                                 'name' => $cat->name,
//                                 'id' => $cat_id,
//                                 'link' => get_term_link($cat_id, 'product_cat'),
//                                'img' => wp_get_attachment_image_src($img_id),
//								);
//							$product_categories = get_categories($args);


?>




<?php
$products_new = wc_get_products([
    'limit' => 10,
    'orderby' => 'date',
    'order' => 'DESC'
]);



$data = [];



$data['lancamentos'] = format_produtos($products_new);


//$data['banner'] = format_products($products_banner);
$home_id = get_the_ID();

$categoria = get_post_meta($home_id, 'categoria', true);

function get_product_category_data($category) {
    $cat = get_term_by('slug', $category, 'product_cat');
    $cat_id = $cat->term_id;
    $img_id = get_term_meta($cat_id, 'thumbnail_id', true);
    return [
        'name' => $cat->name,
        'id' => $cat_id,
        'link' => get_term_link($cat_id, 'product_cat'),
        'img' => wp_get_attachment_image_src($img_id),
    ];
}

$data['categorias'][$categoria] = get_product_category_data($categoria);



?>



    <?php

    $args = array(
        'taxonomy'   => "product_cat",
        'hide_empty' => false,
        'exclude'=>array(),
        'parent' => 0,
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $product_categories = get_categories($args);




    ?>


    <div class="container container-dstq-prod w-container">
        <div class="box-dstq-left w-clearfix">
            <?php// foreach($data['categorias'] as $categoria) { ?>
            <div class="dstq-g" data-ix="hover-dstq-produtos">
                <img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo7.gif" alt="" class="img-dstq bike">
                <a href="<?php echo get_site_url();?>/categoria-produto/radicais" class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>



            <?php// } ?>
            <div class="dstq-p azul" data-ix="hover-dstq-produtos">
                <img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo12.png" alt="" class="img-dstq">
                <a href="<?php echo get_site_url();?>/categoria-produto/outros"  class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
            <div class="dstq-p cinza" data-ix="hover-dstq-produtos">
                <img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo4.png" alt="" class="img-dstq" data-ix="anima-carneiro">
                <a  href="<?php echo get_site_url();?>/categoria-produto/artesanato" class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
        </div>
        <div class="box-dstq-right w-clearfix">
            <div class="dstq-p2 bg-logo" data-ix="hover-dstq-produtos">
                <img src="<?php bloginfo('template_directory'); ?>/images/logo_animado.gif" alt="" alt="" class="logo-main-banner">
                <a href="#" class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
            <div class="dstq-p2 beje" data-ix="hover-dstq-produtos">
                <img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo10.gif" alt="" class="img-dstq">
                <a href="#" class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
            <div class="dstq-p2" data-ix="hover-dstq-produtos">
                <img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo3.png" srcset="<?php bloginfo('template_directory'); ?>/images/img-brinquedo3-p-500.png 500w, <?php bloginfo('template_directory'); ?>/images/img-brinquedo3.png 692w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 15vw, (max-width: 991px) 109.796875px, 157.5px" alt="" class="img-dstq">
                <a class="hover-nome-prod w-inline-block">
                    <div>Bondinho<br>Desmonte<br>&amp;<br>Encaixe</div>
                </a>
            </div>
            <div class="dstq-p2 amarelo" data-ix="hover-dstq-produtos">
                <img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo2.png" alt="" class="img-dstq" data-ix="anima-frutas">
                <a href="#" class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
        </div>
        <div class="container container-whats-home w-container">
            <a href="https://api.whatsapp.com/send?phone=5511976943311" target="_blank" class="cta-whats-home w-button" id="sobre">
                <strong>Quer falar mais sobre esse produto? Nos chame no WhatsApp!</strong>
            </a>
        </div>



    <div class="section">
        <div class="container cont-who-our w-container">
            <div class="box-institu quem-somos">
                <div class="content-institu fixo">
                    <img src="<?php bloginfo('template_directory'); ?>/images/abre-aspas.png" alt="">
                    <p>A roda vai girar,<br>vai girar<br>Alegria tá no ar, <br>tá no ar.<br><br>Pra rir<br>sem palhaçada<br>tem brincadeira<br>inventada, <br>Muita diversão<br>e risadas.</p>
                    <img src="<?php bloginfo('template_directory'); ?>/images/fecha-aspas.png" alt="">
                </div>
            </div>
            <div class="box-institu responsabilidade" data-ix="hover-box-institu">
                <h2>quem<br>somos</h2>
                <div class="content-institu">
                    <p>A criança que fomos. A infância que queremos para nossos filhos.<br><br>A Roda Gigante é assim, inocência e fantasia por todas as prateleiras. Oferecemos brinquedos cheios de alegria de brincar e de imaginar, para as crianças se divertirem desconectadas dos eletrônicos, só plugadas nas mais divertidas ideias.</p>
                </div>
            </div>
            <div class="box-institu historia" data-ix="hover-box-institu">
                <h2>Nossa<br>história</h2>
                <div class="content-institu">
                    <p>A maternidade não nos torna apenas mães, ela nos envolve com a magia de novas descobertas e sentimentos inexplicáveis. É como uma roda gigante, sonhamos e sentimos aquele frio na barriga, jamais imaginávamos como seria tão incrível chegar lá em cima. “De novo!”, diria a criança logo depois da descida com aquele sorriso maravilhado no rosto.</p>
                    <p>Deixei a carreira de publicitaria para me dedicar a esse universo lindo e mágico dos brinquedos educativos. Fazer o sorriso brotar em uma criança, ver o brilho no olhar é o que me completa.</p>
                    <p>Por toda a loja, em cada detalhe, coloco meu carinho e gesto amoroso de mãe. Bem-vindos. Entre nessa roda e vamos voltar no tempo.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-sustentabilidade">
        <div class="container container-sustenta w-container">
            <div class="cercado-cima"></div>
            <img src="<?php bloginfo('template_directory'); ?>/images/ico-arvore.png" alt="">
            <h2 class="h2-sustentabilidade" >Brinquedos educativos e também sustentáveis.<br><br>Imaginar um mundo melhor é um compromisso. <br id="brinquedos">Valorizamos os brinquedos sustentáveis.</h2>
        </div>
    </div>




    <div class="section">
        <div class="container container-carrossel w-container">
            <h2>nossos brinquedos</h2>
            <div data-animation="slide" data-duration="500" data-infinite="1" class="slider-brinquedos w-slider">
                <div class="mask-slider w-slider-mask">

                    <?php foreach($data['lancamentos'] as $produtos) { ?>

                        <div class="slide w-slide" data-ix="hover-brinquedos-carrossel">
                            <a class="box-img-slide w-inline-block">
                                <img src="<?= $produtos['img'][0]; ?>" alt="" class="img-slide">
                            </a>
                            <div class="txt-slide"><?= $produtos['name']?></div>
                        </div>


                    <?php } ?>

                    <!--                <div class="slide w-slide" data-ix="hover-brinquedos-carrossel">-->
                    <!--                    <a href="--><?php //bloginfo('template_directory');  ?><!--?>" class="box-img-slide w-inline-block">-->
                    <!--                    <img src="--><?php //bloginfo('template_directory'); ?><!--/images/img-brinquedo-carrosel2.jpg" alt="" class="img-slide">-->
                    <!--                </a>-->
                    <!--                    <div class="txt-slide">Woodbike 2 em 1</div>-->
                    <!--                </div>-->
                    <!---->
                    <!---->
                    <!--                <div class="slide w-slide" data-ix="hover-brinquedos-carrossel"><a class="box-img-slide w-inline-block">-->
                    <!--                    <img src="--><?php //bloginfo('template_directory'); ?><!--/images/img-brinquedo-carrosel3.jpg" alt="" class="img-slide">-->
                    <!--                </a>-->
                    <!--                    <div class="txt-slide">leåo</div>-->
                    <!--                </div>-->
                    <!---->
                    <!--                -->
                    <!--                <div class="slide w-slide" data-ix="hover-brinquedos-carrossel"><a class="box-img-slide w-inline-block">-->
                    <!--                    <img src="--><?php //bloginfo('template_directory'); ?><!--/images/img-brinquedo-carrosel4.jpg" alt="" class="img-slide">-->
                    <!--                </a>-->
                    <!--                    <div class="txt-slide">foguete</div>-->
                    <!--                </div>-->
                    <!---->
                    <!---->
                    <!--                <div class="slide w-slide" data-ix="hover-brinquedos-carrossel">-->
                    <!--                    <a class="box-img-slide w-inline-block">-->
                    <!--                        <img src="--><?php //bloginfo('template_directory'); ?><!--/images/img-brinquedo-carrosel5.jpg" alt="" class="img-slide">-->
                    <!--                    </a>-->
                    <!--                    <div class="txt-slide">bonecos</div>-->
                    <!--                </div>-->
                    <!---->
                    <!--                <div class="slide w-slide" data-ix="hover-brinquedos-carrossel">-->
                    <!--                    <a class="box-img-slide w-inline-block">-->
                    <!--                        <img src="--><?php //bloginfo('template_directory'); ?><!--/images/img-brinquedo-carrosel5.jpg" alt="" class="img-slide">-->
                    <!--                    </a>-->
                    <!--                    <div class="txt-slide">bonecos</div>-->
                    <!--                </div>-->


                </div>

                <div class="arrow-slider w-slider-arrow-left">
                    <div class="w-icon-slider-left"></div>
                </div>
                <div class="arrow-slider w-slider-arrow-right">
                    <div class="w-icon-slider-right"></div>
                </div>
                <div class="w-hidden-main w-hidden-medium w-hidden-small w-hidden-tiny w-slider-nav w-round"></div>
            </div>
        </div>
    </div>
    <div>

        <!-- <div class="container container-dstq-prod w-container">
        <div class="box-dstq-left w-clearfix">
            <div class="dstq-g" data-ix="hover-dstq-produtos"><img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo7.gif" alt="" class="img-dstq bike">
                <a class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
            <div class="dstq-p azul" data-ix="hover-dstq-produtos"><img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo12.png" alt="" class="img-dstq">
                <a class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
            <div class="dstq-p cinza" data-ix="hover-dstq-produtos"><img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo4.png" alt="" class="img-dstq" data-ix="anima-carneiro">
                <a class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
        </div>
        <div class="box-dstq-right w-clearfix">
            <div class="dstq-g2" data-ix="hover-dstq-produtos"><img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo10.gif" alt="" class="img-dstq">
                <a class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
            <div class="dstq-p2" data-ix="hover-dstq-produtos"><img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo3.png" srcset="<?php bloginfo('template_directory'); ?>/images/img-brinquedo3-p-500.png 500w, <?php bloginfo('template_directory'); ?>/images/img-brinquedo3.png 692w" sizes="(max-width: 479px) 100vw, (max-width: 767px) 15vw, (max-width: 991px) 109.796875px, 157.5px" alt="" class="img-dstq">
                <a class="hover-nome-prod w-inline-block">
                    <div>Bondinho<br>Desmonte<br>&amp;<br>Encaixe</div>
                </a>
            </div>
            <div class="dstq-p2 amarelo" data-ix="hover-dstq-produtos"><img src="<?php bloginfo('template_directory'); ?>/images/img-brinquedo2.png" alt="" class="img-dstq" data-ix="anima-frutas">
                <a class="hover-nome-prod w-inline-block">
                    <div></div>
                </a>
            </div>
        </div>
    </div> -->
    </div>
    <div>
        <div class="container container-diferenciais w-container">
            <div class="content-frases w-clearfix">
                <div class="box-dif">
                    <div>Quem quiser que apareça<br>Sem bilhete, nem senha<br>Aceite o convite e VENHA!</div>
                </div>
                <div class="box-dif">
                    <div>Criançada, meninada<br>vem querer, vem brincar.<br>ou vão ficar no celular?</div>
                </div>
                <div class="box-dif">
                    <div>Garotada, gurizada<br>vem sonhar, vai valer<br>ou já grudaram na TV?</div>
                </div>
            </div>
            <div class="frase-solta" id="contato">A roda vai girar, vai girar Alegria tá no ar, tá no ar.</div>
        </div>
    </div>
    <div>
    <div class="container container-contato w-container">
    <div class="box-img-contado w-hidden-tiny"></div>
    <div class="form-contato w-form">
    <form id="wf-form-contato" name="form-contato-footer" method="post" action="<?php echo get_template_directory_uri();?>/enviar.php" class="formphp">
        <img src="<?php bloginfo('template_directory'); ?>/images/ico-faleconosco.png">
        <h3 class="h3-contato">Estamos aqui para te ouvir e fazer novos amigos</h3>
        <input type="text" class="input w-input" maxlength="256" name="nome-contato-footer" placeholder="Nome" id="nome-contato-footer" required>
        <input type="email" class="input w-input" maxlength="256" name="email-contato-footer" placeholder="e-mail" id="email-contato-footer" required>
        <!-- <input type="text" class="input w-input" maxlength="256" name="assunto-contato-footer" placeholder="Assunto" id="assunto-contato-footer" required> -->
        <select id="assunto-contato-footer" name="assunto-contato-footer" class="select w-select" required>
        <option value="" selected="">Selecione o assunto</option>
                <option value="Dúvidas">Dúvidas</option>
                <option value="Sugestões">Sugestões</option>
                <option value="Reclamações">Reclamações</option>
                <option value="Parcerias">Parcerias</option>
                <option value="Elogios">Elogios</option>
           </select>
           

           <label class="nao-aparece">Se você não é um robô, deixe em branco.</label>
                        <input type="text" class="nao-aparece" name="leaveblank">

                        <label class="nao-aparece">Se você não é um robô, não mude este campo.</label>
                        <input type="text" class="nao-aparece" name="dontchange" value="http://">
        <textarea id="mensagem-contato-footer" name="mensagem-contato-footer" placeholder="MENSAGEM" maxlength="5000" class="text-area w-input" required></textarea>
        <input type="submit" value="enviar" class="submit-form w-button" id="enviar-contato-footer">

        <div id="processando"></div>
        <div id="msgm-envio"></div>

        <div class="info-form">
            E-mail: <a href="mailto:contato@rodagigantebrinquedos.com.br?subject=Contato%20Roda%20Gigante" class="email-form">contato@rodagigantebrinquedos.com.br</a><br>
            Telefone: <a href="tel:+55113676-0388" class="tel-form">(11) 3676-0388</a><br>
            Whatsapp: <a href="https://api.whatsapp.com/send?phone=5511976943311" target="_blank" class="tel-form">(11) 97694-3311</a><br>
            Endereço: Rua Desembargador do Vale, 203 <br>Perdizes, São Paulo - SP <br>05010-040
        </div>
    </form>



    </div>
    </div>
    </div>


<?php get_footer(); ?>
