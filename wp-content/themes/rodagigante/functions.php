<?php

/**
 * @ Rai
 * removes admin bar in the frontend
 */
add_filter('show_admin_bar', '__return_false');

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
    echo '<section id="main">';
}

function my_theme_wrapper_end() {
    echo '</section>';
}

/**
 * @author Rai
 * adds support to Woocommerce  in the theme
 */
function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce', array(
        //'thumbnail_image_width' => 150,
        //'single_image_width'    => 300,

        'product_grid'          => array(
            'default_rows'    => 3,
            'min_rows'        => 2,
            'max_rows'        => 8,
            'default_columns' => 4,
            'min_columns'     => 2,
            'max_columns'     => 5,
        ),
    ) );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_theme_support( 'wc-product-gallery-zoom' );


/**
 * @author Rai
 * disables the automatic woocommerce sidebar
 */
function disable_woo_commerce_sidebar() {
    remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
}
add_action('init', 'disable_woo_commerce_sidebar');


/**
 * @author Rai
 * Display the custom text field  - Fabricante
 */
function cfwc_create_custom_field_fabrincante() {

    //get the value of the field to show in the admin
    $preenche_fabricante = get_post_meta( get_the_id(), 'custom_text_field_title_fabricante', true );

    $args = array(
        'id' => 'fabricante',
        'label' => __( 'Fabricante do produto', 'cfwc_fabricante' ),
        'class' => 'cfwc-custom-field',
        'desc_tip' => true,
        'description' => __( 'Nome do frabricante do produto.', 'cfwc_fabricante' ),
        'value' => $preenche_fabricante
    );
    woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'cfwc_create_custom_field_fabrincante' );


/**
 * @author Rai
 * Save the custom field - Fabricante
 */
function cfwc_save_custom_field_fabricante( $post_id ) {
    $product = wc_get_product( $post_id );
    $title = isset( $_POST['fabricante'] ) ? $_POST['fabricante'] : '';
    $product->update_meta_data( 'custom_text_field_title_fabricante', sanitize_text_field( $title ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field_fabricante' );


/**
 * @author Rai
 * Display the custom text field  - Material
 */
function cfwc_create_custom_field_material() {

    //get the value of the field to show in the admin
    $preenche_material = get_post_meta( get_the_id(), 'custom_text_field_title_material', true );

    $args = array(
        'id' => 'material',
        'label' => __( 'Material do produto', 'cfwc_material' ),
        'class' => 'cfwc-custom-field',
        'desc_tip' => true,
        'description' => __( 'Material do produto.', 'cfwc_material' ),
        'value' => $preenche_material
    );
    woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'cfwc_create_custom_field_material' );


/**
 * @author Rai
 * Save the custom field - Material
 */
function cfwc_save_custom_field_material( $post_id ) {
    $product = wc_get_product( $post_id );
    $title = isset( $_POST['material'] ) ? $_POST['material'] : '';
    $product->update_meta_data( 'custom_text_field_title_material', sanitize_text_field( $title ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field_material' );


/**
 * @author Rai
 * Display the custom text field  - Inmetro
 */
function cfwc_create_custom_field_inmetro() {
    //get the value of the field to show in the admin
    $preenche_inmetro = get_post_meta( get_the_id(), 'custom_text_field_title_inmetro', true );

    $args = array(
        'id' => 'inmetro',
        'label' => __( 'Informações Inmetro', 'cfwc_inmetro' ),
        'class' => 'cfwc-custom-field',
        'desc_tip' => true,
        'description' => __( 'Informações do Inmetro.', 'cfwc_inmetro' ),
        'value' => $preenche_inmetro
    );
    woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'cfwc_create_custom_field_inmetro' );


/**
 * @author Rai
 * Save the custom field - Inmetro
 */
function cfwc_save_custom_field_inmetro( $post_id ) {
    $product = wc_get_product( $post_id );
    $title = isset( $_POST['inmetro'] ) ? $_POST['inmetro'] : '';
    $product->update_meta_data('custom_text_field_title_inmetro', sanitize_text_field( $title ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field_inmetro' );


/**
 * @author Rai
 * Display the custom text field  - Dimensões da caixa
 */
function cfwc_create_custom_field_caixa() {
    //get the value of the field to show in the admin - comprimento
    $preenche_caixa_comprimento = get_post_meta( get_the_id(), 'custom_text_field_title_caixa_comprimento', true );

    $args = array(
        'id' => 'caixa_comprimento',
        'label' => __( 'Comprimento da caixa', 'cfwc_caixa_comprimento' ),
        'class' => 'cfwc-custom-field',
        'desc_tip' => true,
        'description' => __( 'Dimensões da caixa - comprimento.', 'cfwc_caixa_comprimento' ),
        'value' => $preenche_caixa_comprimento
    );
    woocommerce_wp_text_input( $args );

    //get the value of the field to show in the admin - largura
    $preenche_caixa_largura = get_post_meta( get_the_id(), 'custom_text_field_title_caixa_largura', true );

    $args = array(
        'id' => 'caixa_largura',
        'label' => __( 'Largura da caixa', 'cfwc_caixa_largura' ),
        'class' => 'cfwc-custom-field',
        'desc_tip' => true,
        'description' => __( 'Largura da caixa.', 'cfwc_caixa_largura' ),
        'value' => $preenche_caixa_largura
    );
    woocommerce_wp_text_input( $args );

    //get the value of the field to show in the admin - altura
    $preenche_caixa_altura = get_post_meta( get_the_id(), 'custom_text_field_title_caixa_altura', true );

    $args = array(
        'id' => 'caixa_altura',
        'label' => __( 'Altura da caixa', 'cfwc_caixa_altura' ),
        'class' => 'cfwc-custom-field',
        'desc_tip' => true,
        'description' => __( 'Altura da caixa.', 'cfwc_caixa_altura' ),
        'value' => $preenche_caixa_altura
    );
    woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_shipping', 'cfwc_create_custom_field_caixa' );


/**
 * @author Rai
 * Save the custom field - Dimensões da caixa Comprimento
 */
function cfwc_save_custom_field_caixa_comprimento( $post_id ) {
    $product = wc_get_product( $post_id );
    $caixa_comprimento = isset( $_POST['caixa_comprimento'] ) ? $_POST['caixa_comprimento'] : '';
    $product->update_meta_data('custom_text_field_title_caixa_comprimento', sanitize_text_field( $caixa_comprimento ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field_caixa_comprimento' );


/**
 * @author Rai
 * Save the custom field - Dimensões da caixa Largura
 */
function cfwc_save_custom_field_caixa_largura( $post_id ) {
    $product = wc_get_product( $post_id );
    $caixa_largura = isset( $_POST['caixa_largura'] ) ? $_POST['caixa_largura'] : '';
    $product->update_meta_data('custom_text_field_title_caixa_largura', sanitize_text_field( $caixa_largura ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field_caixa_largura' );



/**
 * @author Rai
 * Save the custom field - Dimensões da caixa Altura
 */
function cfwc_save_custom_field_caixa_altura( $post_id ) {
    $product = wc_get_product( $post_id );
    $caixa_altura = isset( $_POST['caixa_altura'] ) ? $_POST['caixa_altura'] : '';
    $product->update_meta_data('custom_text_field_title_caixa_altura', sanitize_text_field( $caixa_altura ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field_caixa_altura' );


function register_my_menu() {
    register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );



function format_products($produtos) {
    $produtos_final = [];
    foreach($produtos as $produtos) {
        $produtos_final[] = [
            'name' => $produtos->get_name(),
            'price' => $produtos->get_price_html(),
            'link' => $produtos->get_permalink(),
            'img' => wp_get_attachment_image_src($produtos->get_image_id()),
        ];
    }
    return $produtos_final;
}

function roda_product_list($produtos) { ?>
    <ul class="products-list">
        <?php foreach($produtos as $produtos) { ?>
            <li class="product-item">
                <a href="<?= $produtos['link']; ?>">
                    <div class="product-info">
                        <img src="<?= $produtos['img']; ?>" alt="<?= $produtos['name']; ?>">
                        <h2><?= $produtos['name']; ?> - <span><?= $produtos['price']; ?></span></h2>
                    </div>
                    <div class="product-overlay">
                        <span class="btn-link">Ver Mais</span>
                    </div>
                </a>
            </li>
        <?php } ?>
    </ul>
    <?php
} // Fecha a função handel





function format_produtos($produtos){

    $produtos_final =[];
    
    foreach($produtos as $item){
        $produtos_final[] = [
            'name' => $item->get_name(),
            
            'preco' => $item->get_price_html(),
            'link' => $item->get_permalink( ),
            'img' => wp_get_attachment_image_src($item->get_image_id()),
            
           

            //'price' => ,

        ];

    }
    return $produtos_final;
}



function rodagigante_loop_per_page(){
    return 6;
}

add_filter('loop_shop_per_page' ,'rodagigante_loop_per_page' );




function wordpress_pagination() {
    global $wp_query;

    $big = 12;

    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages
    ) );
}


function primeiroBanner(){


    $args = array(
        'taxonomy'   => "product_cat",
        'hide_empty' => false,
        'exclude'=>array(),
        'parent' => 0,
        'orderby' => 'title',
        'order' => 'ASC'
    );
    $product_categories = get_categories($args);




?>


    <?php } ?>




