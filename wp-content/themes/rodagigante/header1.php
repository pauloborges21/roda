<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Wed Oct 10 2018 21:44:10 GMT+0000 (UTC)  -->
<html data-wf-page="5b9bfa63efe50a09690f69b9" data-wf-site="5b9a7a40c03054f6cc545866">
<head>
    <meta charset="utf-8">
    <title>roda-gigante</title>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="Webflow" name="generator">
    <link href="<?php bloginfo('template_directory'); ?>/css/normalize.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/css/webflow.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/css/roda-gigante.webflow_old2.css" rel="stylesheet" type="text/css">
    <link href="<?php bloginfo('template_directory'); ?>/css/adicional.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" type="text/javascript" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js" type="text/javascript"></script>
    <script type="text/javascript">WebFont.load({  google: {    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic","Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"]  }});</script>
    <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
    <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
    <link href="<?php bloginfo('template_directory'); ?>/images/favicon.png" rel="shortcut icon" type="image/x-icon">
    <link href="<?php bloginfo('template_directory'); ?>/images/webclip.png" rel="apple-touch-icon">
    <style type="text/css">
    @media screen and (min-width: 1280px) {
    .w-container {
        max-width: 1640px;
            }
            .nav-menu {
        font-size: 18px;
            }
            .nav-link {
        margin-right: 30px;
                margin-left: 30px;
            }
            .li-icos-nav {
        margin-right: 20px;
                margin-left: 20px;
                font-size: 13px;
                font-weight: 600;
            }
            .container.container-main-banner {
        background-position: 50% 65%;
            }
            .logo-main-banner {
        width: 450px;
            }
            .box-institu {
        height: 450px;
            }
            .content-institu.fixo {
        font-size: 20px;
                line-height: 30px;
                letter-spacing: 15px;
            }
            h2 {
        font-size: 22px;
                line-height: 40px;
                font-weight: 500;
                color: #707070;
                letter-spacing: 5px;
            }
            .content-institu {
        padding-top: 110px;
                font-size: 15px;
                line-height: 20px;
            }
            .h2-sustentabilidade {
        letter-spacing: 10px;
                font-size: 20px;
            }
            .txt-slide {
        font-size: 15px;
                line-height: 17px;
            }
            .box-dstq-left, .box-dstq-right {
        height: 730px;
            }
            .dstq-g {
                height: 420px;
                background-position: 50% 50%;
                background-size: auto 350px;
            }
            .img-dstq {
        max-width: 100%;
                width: 70%;
            }
            .hover-nome-prod {
        font-size: 20px;
                line-height: 40px;
            }
            .dstq-p {
        height: 280px;
            }
            .dstq-g2 {
        height: 380px;
            }
            .dstq-p2 {
        height: 350px;
            }
            .content-prod-det {
        width: 55%;
    }
            .box-dif {
        line-height: 50px;
                font-weight: 500;
                font-size: 20px;
                letter-spacing: 3px;
                height: 325px;
            }
        }
        select {
    -webkit-appearance: none !important; /*Removes default chrome and safari style*/
            -moz-appearance: none !important; /*Removes default style Firefox*/
            -ms-appearance: none;
            -o-appearance:none !important;
            appearance:none !important;
            overflow:hidden;
            width: 120%;
        }
        select::-ms-expand {
            display: none !important;
        }
        .select-cadastro::-ms-expand {
            display: none !important;
        }
    </style>
</head>
<body>

<?php// if(is_front_page()){ ?>
<div class="section section-header">
    <div class="container"></div>
    <div class="container w-container">
        <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
            <div class="container-nav w-container">
            <nav role="navigation" class="nav-menu w-nav-menu"  style="font-size:14px">
                <a href="#sobre" class="nav-link w-nav-link">Quem Somos</a>
                <a href="#sobre" class="nav-link w-nav-link">Responsabilidade Social</a>
                <a href="#sobre" class="nav-link w-nav-link">Nossa história</a>
                <a href="#brinquedos" class="nav-link w-nav-link">Brinquedos</a>
                <a href="#oficinas" class="nav-link w-nav-link">Oficinas da Roda</a>
                <a href="#contato" class="nav-link w-nav-link">Contato</a>
                </nav>
                <div class="menu-btn w-nav-button">
                    <div class="w-icon-nav-menu"></div>
                </div>
                <div class="box-icos-nav">
                    <a href="https://api.whatsapp.com/send?phone=5511976943311" target="_blank" class="btn-whats">Quer falar mais sobre os produtos? Nos chame no WhatsApp!</a>
                </div>
                <div class="box-icos-nav" style="display: none">
                    <ul class="ul-icos-nav w-clearfix w-list-unstyled">
                        <li class="li-icos-nav">
                            <a href="#" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-lupa.png" alt="">
                                <div>pesquisar</div>
                            </a>
                        </li>
                        <li class="li-icos-nav">
                            <a href="/minha-conta/" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-login.png" alt="">
                                <div>login</div>
                            </a>
                        </li>
                        <li class="li-icos-nav">
                            <a href="/minha-conta/orders/" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-pedidos.png" alt="">
                                <div>pedidos</div>
                            </a>
                        </li>
                        <li class="li-icos-nav">
                            <a href="/carrinho/" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-carrinho.png" alt="">
                                <div>carrinho</div>
                                <div class="qtd-carrinho">
                                    <?php
                                    global $woocommerce;
                                    $count = $woocommerce->cart->cart_contents_count;
                                    ?>
                                    <div><?php echo $count; ?></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <?php /*wp_nav_menu(
                            array(
                                    'theme_location'    => 'header-menu',
                                    'menu_class'        => 'ul-icos-nav w-clearfix w-list-unstyled',
                                    'container'         => 'ul',
                                    'container_class'   => 'container-nav w-container'
                            )
                        );
                    */?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php //}else{ ?>

<!-- <div class="section section-header-interna">
    <div class="container"></div>
    <div class="container w-container">
        <div data-collapse="medium" data-animation="default" data-duration="400" class="navbar w-nav">
            <div class="container-nav container-nav-interna w-container">
                <a href="<?php echo site_url(); ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/logo-footer.png" alt="" class="logo-nav-interna">
                </a>
                <nav role="navigation" class="nav-menu w-nav-menu">
                    <a href="<?php echo site_url(); ?>#sobre" class="nav-link w-nav-link">Quem Somos</a>
                    <a href="<?php echo site_url(); ?>#sobre" class="nav-link w-nav-link">Responsabilidade Social</a>
                    <a href="<?php echo site_url(); ?>#quem-somos" class="nav-link w-nav-link">Nossa história</a>
                    <a href="<?php echo site_url(); ?>#brinquedos" class="nav-link w-nav-link">Brinquedos</a>
                </nav>
                <div class="menu-btn w-nav-button">
                    <div class="w-icon-nav-menu"></div>
                </div>
                <div class="box-icos-nav">
                    <ul class="ul-icos-nav w-clearfix w-list-unstyled">
                        <li class="li-icos-nav">
                            <a href="#" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-lupa.png" alt="">
                                <div>pesquisar</div>
                            </a>
                        </li>
                        <li class="li-icos-nav">
                            <a href="/minha-conta/" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-login.png" alt="">
                                <div>login</div>
                            </a>
                        </li>
                        <li class="li-icos-nav">
                            <a href="/minha-conta/orders/" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-pedidos.png" alt="">
                                <div>pedidos</div>
                            </a>
                        </li>
                        <li class="li-icos-nav">
                            <a href="/carrinho/" class="link-ico-nav w-inline-block">
                                <img src="<?php bloginfo('template_directory'); ?>/images/ico-carrinho.png" alt="">
                                <div>carrinho</div>
                                <div class="qtd-carrinho">
                                    <?php
                                    global $woocommerce;
                                    $count = $woocommerce->cart->cart_contents_count;
                                    ?>
                                    <div><?php echo $count; ?></div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> -->

<?php //} ?>